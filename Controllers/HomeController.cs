﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using sm_coding_challenge.Business;
using sm_coding_challenge.Models;
using sm_coding_challenge.Services.DataProvider;

namespace sm_coding_challenge.Controllers
{
    public class HomeController : Controller
    {

        //private IDataProvider _dataProvider;
        private PlayerBusiness _playerBusiness;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        public HomeController(PlayerBusiness playerBusiness, IWebHostEnvironment iWebHostEnvironment)
        {
            _playerBusiness = playerBusiness ?? throw new ArgumentNullException(nameof(playerBusiness));
            _iWebHostEnvironment = iWebHostEnvironment ?? throw new ArgumentNullException(nameof(iWebHostEnvironment));
        }

        public IActionResult Index()
        {
            return View();
        }

        public string sam()
        {
            return _iWebHostEnvironment.EnvironmentName.ToString();
        }

        [HttpGet]
        public async Task<IActionResult> Player(string id)
        {
            try
            {
                var player = await _playerBusiness.GetPlayerById(id);
                return Json(player);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return RedirectToAction(nameof(Error));
            }

        }

        [HttpGet]
        public async Task<IActionResult> Players(string ids)
        {
            try
            {
                var players = await _playerBusiness.GetPlayers(ids);
                if (players == null)
                {
                    return Json("No recound was retrieved. Bad Request.");
                }
                return Json(players);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return RedirectToAction(nameof(Error));
            }

        }

        [HttpGet]
        public async Task<IActionResult> LatestPlayers(string ids)
        {
            try
            {
                var players = await _playerBusiness.LatestPlayers(ids);
                return Json(players);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return RedirectToAction(nameof(Error));
            }

        }

        public IActionResult Error(string errorMessage)
        {
            if (_iWebHostEnvironment.EnvironmentName.ToString() == "Development")
            {
                return View(new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Message = $"Could not complete request. {ViewBag.Error}"
                });
            }
            else
            {
                return View(new ErrorViewModel
                {
                    RequestId = string.Empty,
                    Message = "An error occured. Kindly contact the system administrator."
                });
            }

        }
    }
}
