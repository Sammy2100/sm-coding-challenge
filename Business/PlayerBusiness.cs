using System.Linq;
using System.Collections.Generic;
using sm_coding_challenge.Models;
using sm_coding_challenge.Services.DataProvider;
using System.Threading.Tasks;
using System;

namespace sm_coding_challenge.Business
{

    public class PlayerBusiness
    {
        private IDataProvider _dataProvider;
        public PlayerBusiness(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider ?? throw new System.ArgumentNullException(nameof(dataProvider));
        }

        public async Task<PlayerModel> GetPlayerById(string id)
        {
            var dataResponse = await _dataProvider.GetAllPlayers();

            //==  Changed all "For Loops" to "Linq expression"  ==//

            PlayerModel rushing = dataResponse.Rushing
                .FirstOrDefault(p => p.Id.Equals(id));
            if (rushing != null)
                return rushing;

            PlayerModel passing = dataResponse.Passing
                .FirstOrDefault(p => p.Id.Equals(id));
            if (passing != null)
                return passing;

            PlayerModel receiving = dataResponse.Receiving
                .FirstOrDefault(p => p.Id.Equals(id));
            if (receiving != null)
                return receiving;

            PlayerModel kicking = dataResponse.Kicking
                .FirstOrDefault(p => p.Id.Equals(id));
            if (kicking != null)
                return kicking;

            return null;
        }

        public async Task<List<PlayerModel>> GetPlayers(string ids)
        {
            List<string> playerIds = ids.Split(',').ToList();

            var dataResponse = await _dataProvider.GetAllPlayers();

            //==  Changed all "For Loops" to "Linq to SQL statement"  ==//

            var rushing = (from p in dataResponse.Rushing
                           where ids.Contains(p.Id)
                           select p).ToList();

            if (rushing!.Count > 0)
                return rushing;

            var passing = (from p in dataResponse.Passing
                           where ids.Contains(p.Id)
                           select p).ToList();
            if (passing!.Count > 0)
                return passing;

            var receiving = (from p in dataResponse.Receiving
                             where ids.Contains(p.Id)
                             select p).ToList();
            if (receiving!.Count > 0)
                return receiving;

            var kicking = (from p in dataResponse.Kicking
                           where ids.Contains(p.Id)
                           select p).ToList();
            if (kicking!.Count > 0)
                return kicking;

            return null;

        }

        public async Task<object> LatestPlayers(string playerIds)
        {
            List<string> ids = playerIds.Split(',').ToList();

            var dataResponse = await _dataProvider.GetAllPlayers();

            // Create a new object on the fly to return the structure
            // specified in the exercise
            var result = new
            {
                rushing = dataResponse.Rushing.Where(p => ids.Contains(p.Id)).ToList(),
                receiving = dataResponse.Receiving.Where(p => ids.Contains(p.Id)).ToList(),
                passing = dataResponse.Passing.Where(p => ids.Contains(p.Id)).ToList(),
                kicking = dataResponse.Kicking.Where(p => ids.Contains(p.Id)).ToList()
            };

            return result;
        }
    }
}