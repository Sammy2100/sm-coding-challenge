using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using sm_coding_challenge.Models;
using Microsoft.Extensions.Caching.Memory;

namespace sm_coding_challenge.Services.DataProvider
{
    public class DataProviderImpl : IDataProvider
    {
        private TimeSpan _timeout = TimeSpan.FromSeconds(30);
        private string _url = "https://gist.githubusercontent.com/RichardD012/a81e0d1730555bc0d8856d1be980c803/raw/3fe73fafadf7e5b699f056e55396282ff45a124b/basic.json";
        // moved the URL from the method that calls it to a variable within the class file
        // A better way to do this is to move it to a confid (app setting) file.
        private IMemoryCache _cache;
        private string _cacheKey = "myCachekey";

        public DataProviderImpl(IMemoryCache cache)
        {
            _cache = cache;
            // Injecting the memory cache interface to cache data from the url for 7days
        }

        // Seperated the block of code that pulls data from the api into a public method
        // This because irrespective of the parameter(s) passed, the all players' data are pulled from the api.
        // This is made private incase the data source is changed or simply to hide the data source
        // method name from the consumer (business layer or service layer)
        public async Task<DataResponseModel> GetPlayerData()
        {

            // Check if result was peviously cached.
            if (!_cache.TryGetValue(_cacheKey, out DataResponseModel result))
            {
                var handler = new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                };

                using (var client = new HttpClient(handler) { Timeout = _timeout })
                {
                    var dataString = await client.GetStringAsync(_url);

                    result = JsonConvert.DeserializeObject<DataResponseModel>(dataString,
                        new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto }
                    );

                    // Cache the result for 7 days.
                    // The value "7" may need to be moved to a config (app setting) file
                    _cache.Set(_cacheKey, result, new MemoryCacheEntryOptions()
                    {
                        AbsoluteExpiration = DateTime.Now.AddDays(7)
                    });
                }
            }
            return result;
        }


        // This is created simply to hide the method with the data source implementation from
        // its consumer and provide a generic method name the consumer can easily relate with
        public async Task<DataResponseModel> GetAllPlayers()
        {
            return await GetPlayerData();
        }
    }
}
