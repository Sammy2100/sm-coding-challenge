using System.Collections.Generic;
using System.Threading.Tasks;
using sm_coding_challenge.Models;

namespace sm_coding_challenge.Services.DataProvider
{
    public interface IDataProvider
    {
        // Removed the "GetPlayerById" interface as it is no longer needed
        // Task<PlayerModel> GetPlayerById(string id);
        // This has been replaced "GetAllPlayers" because irrespective of 
        // the parameter(s) passed, the all player data are pulled from the api.
        Task<DataResponseModel> GetAllPlayers();
    }
}
